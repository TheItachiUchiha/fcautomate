package com.infy.fc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.infy.fc.VO.AttributesVO;
import com.infy.fc.VO.FoodCourtsVO;
import com.infy.fc.VO.ItemTypeVO;
import com.infy.fc.VO.ItemVO;
import com.infy.fc.VO.VendorsVO;
import com.infy.fc.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/user", method=RequestMethod.GET)
	public String home(ModelMap model){
		model.addAttribute("locations", userService.getLocations());
		model.addAttribute("items", getItems());
		model.addAttribute("itemTypes", getItemTypes());
		return "user/index";
	}
	
	@RequestMapping(value="/user/getFC", method=RequestMethod.POST)
	public @ResponseBody List<FoodCourtsVO> getFC(@RequestParam("id")int dcID) {
		List<FoodCourtsVO> listOfFC = userService.getFCs(dcID);
		return listOfFC;
	}
	
	@RequestMapping(value="/user/getVendors", method=RequestMethod.POST)
	public @ResponseBody List<VendorsVO> getVendors(@RequestParam("fcID")int fcId) {
		List<VendorsVO> listOfVendors = userService.getVendors(fcId);
		return listOfVendors;
	}
	
	private List<ItemVO> getItems(){
		List<ItemVO> list = new ArrayList<ItemVO>();
		
		/*Meals meals = new Meals();
		meals.setMealsId(1);
		meals.setMealsName("BreakFast");*/
		
		ItemVO itemVO = new ItemVO();
		itemVO.setItemId(1);
		itemVO.setName("Mini North Meal");
		itemVO.setPrice(50);
		itemVO.setVeg(true);
		itemVO.setIsAvailable('Y');
		itemVO.setItemType(getItemTypes().get(0));
		
		AttributesVO attributesVO1 = new AttributesVO();
		attributesVO1.setAttributeId(1);
		attributesVO1.setAttributeName("Rice");
		
		AttributesVO attributesVO2 = new AttributesVO();
		attributesVO2.setAttributeId(1);
		attributesVO2.setAttributeName("Rice");
		
		List<AttributesVO> attributes = new ArrayList<AttributesVO>();
		itemVO.setAttributes(attributes);
		
		ItemVO itemVO1 = new ItemVO();
		itemVO1.setItemId(2);
		itemVO1.setName("Paneer With Chapati");
		itemVO1.setPrice(50);
		itemVO1.setItemType(getItemTypes().get(1));
		list.add(itemVO);
		list.add(itemVO1);
		
		return list;
		
	}
	
	public List<ItemTypeVO> getItemTypes(){
		List<ItemTypeVO> listOfItemTypes = new ArrayList<ItemTypeVO>();
		ItemTypeVO meals = new ItemTypeVO();
		meals.setId(1);
		meals.setName("North Meal");
		
		ItemTypeVO comboMeals = new ItemTypeVO();
		comboMeals.setId(2);
		comboMeals.setName("Combo Meal");
		
		listOfItemTypes.add(meals);
		listOfItemTypes.add(comboMeals);
		return listOfItemTypes;
	}

}
