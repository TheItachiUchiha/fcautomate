package com.infy.fc.VO;

import java.util.List;

import com.infy.fc.dao.entity.ItemType;
import com.infy.fc.dao.entity.Meals;
import com.infy.fc.dao.entity.VendorFoodCourtMap;

public class ItemVO {

	private int itemId;
	private VendorFoodCourtMap  foodCourtMap;
	private ItemTypeVO itemType;
	private Meals meals;
	private boolean veg;
	private String name;
	private char isAvailable;
	private float price;
	private List<AttributesVO> attributes;
 
	public int getItemId() {
		return itemId;
	}
	public boolean isVeg() {
		return veg;
	}
	public void setVeg(boolean veg) {
		this.veg = veg;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public VendorFoodCourtMap getFoodCourtMap() {
		return foodCourtMap;
	}
	public void setFoodCourtMap(VendorFoodCourtMap foodCourtMap) {
		this.foodCourtMap = foodCourtMap;
	}
	public ItemTypeVO getItemType() {
		return itemType;
	}
	public void setItemType(ItemTypeVO itemType) {
		this.itemType = itemType;
	}
	public Meals getMeals() {
		return meals;
	}
	public void setMeals(Meals meals) {
		this.meals = meals;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(char isAvailable) {
		this.isAvailable = isAvailable;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public List<AttributesVO> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<AttributesVO> attributes) {
		this.attributes = attributes;
	}

}
