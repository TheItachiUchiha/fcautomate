package com.infy.fc.VO;

import com.infy.fc.dao.entity.ItemType;
import com.infy.fc.dao.entity.Meals;
import com.infy.fc.dao.entity.VendorFoodCourtMap;

public class FoodItemsVO {


	private int itemId;
	
	private VendorFoodCourtMap  foodCourtMap;
	
	private ItemType itemType;
	
	private Meals meals;
	
	private boolean veg;

	private String itemName;
	private char isAvailable;
	private float itemPrice;
	public int getItemId() {
		return itemId;
	}
	
	public boolean isVeg() {
		return veg;
	}
	public void setVeg(boolean veg) {
		this.veg = veg;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public VendorFoodCourtMap getFoodCourtMap() {
		return foodCourtMap;
	}
	public void setFoodCourtMap(VendorFoodCourtMap foodCourtMap) {
		this.foodCourtMap = foodCourtMap;
	}
	public ItemType getItemType() {
		return itemType;
	}
	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}
	public Meals getMeals() {
		return meals;
	}
	public void setMeals(Meals meals) {
		this.meals = meals;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public char getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(char isAvailable) {
		this.isAvailable = isAvailable;
	}
	public float getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(float itemPrice) {
		this.itemPrice = itemPrice;
	}
	


}
