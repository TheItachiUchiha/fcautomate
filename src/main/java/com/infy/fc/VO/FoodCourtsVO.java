package com.infy.fc.VO;



import com.infy.fc.dao.entity.Location;

public class FoodCourtsVO {

	private int foodCourtId;
	
	private String foodCourtName;
	
	private Location location;

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getFoodCourtId() {
		return foodCourtId;
	}

	public void setFoodCourtId(int foodCourtId) {
		this.foodCourtId = foodCourtId;
	}

	public String getFoodCourtName() {
		return foodCourtName;
	}

	public void setFoodCourtName(String foodCourtName) {
		this.foodCourtName = foodCourtName;
	}



}
