package com.infy.fc.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FOODCOURT")
public class FoodCourt {

	@Id
	@Column(name = "FC_ID")
	 @GeneratedValue(strategy = GenerationType.AUTO)
	private int foodCourtId;
	
	@Column(name = "FC_NAME")
	private String foodCourtName;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "DC_ID")
	private Location location;

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getFoodCourtId() {
		return foodCourtId;
	}

	public void setFoodCourtId(int foodCourtId) {
		this.foodCourtId = foodCourtId;
	}

	public String getFoodCourtName() {
		return foodCourtName;
	}

	public void setFoodCourtName(String foodCourtName) {
		this.foodCourtName = foodCourtName;
	}

}
