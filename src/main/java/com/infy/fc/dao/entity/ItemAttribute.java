package com.infy.fc.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="ITEM_ATTRIBUTES")
public class ItemAttribute {

	@Id
	@Column(name="ID")
	 @GeneratedValue(strategy = GenerationType.AUTO)
	private int itemNum;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ITEM_ID")
	private Item item;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ATTRIBUTE_ID")
	private Attributes attributes;
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public Attributes getAttributes() {
		return attributes;
	}
	public void setAttributes(Attributes attributes) {
		this.attributes = attributes;
	}
	
}
