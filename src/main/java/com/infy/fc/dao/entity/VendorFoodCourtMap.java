package com.infy.fc.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="VENDOR_FOODCOURT")
public class VendorFoodCourtMap {

	@Id
	@Column(name="VENDOR_FC_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int vendorFoodCourtId;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FC_ID")
	private FoodCourt foodCourt;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "VENDOR_ID")
	private Vendor vendor;

	public FoodCourt getFoodCourt() {
		return foodCourt;
	}

	public void setFoodCourt(FoodCourt foodCourt) {
		this.foodCourt = foodCourt;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public int getVendorFoodCourtId() {
		return vendorFoodCourtId;
	}

	public void setVendorFoodCourtId(int vendorFoodCourtId) {
		this.vendorFoodCourtId = vendorFoodCourtId;
	}
	
	
}
