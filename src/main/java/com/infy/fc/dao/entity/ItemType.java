package com.infy.fc.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ITEM_TYPE")
public class ItemType {

	@Id
	@Column(name="ITEM_TYPE_ID")
	 @GeneratedValue(strategy = GenerationType.AUTO)
	private int itemTypeId;
	@Column(name="ITEM_TYPE_NAME")
	private String itemTypeName;
	public int getItemTypeId() {
		return itemTypeId;
	}
	public void setItemTypeId(int itemTypeId) {
		this.itemTypeId = itemTypeId;
	}
	public String getItemTypeName() {
		return itemTypeName;
	}
	public void setItemTypeName(String itemTypeName) {
		this.itemTypeName = itemTypeName;
	}
}
