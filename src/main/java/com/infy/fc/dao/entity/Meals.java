package com.infy.fc.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MEALS")
public class Meals {

	@Id
	@Column(name="MEALS_ID")
	 @GeneratedValue(strategy = GenerationType.AUTO)
	private int  mealsId;
	
	@Column(name="MEALS_NAME")
	private String mealsName;
	public int getMealsId() {
		return mealsId;
	}
	public void setMealsId(int mealsId) {
		this.mealsId = mealsId;
	}
	public String getMealsName() {
		return mealsName;
	}
	public void setMealsName(String mealsName) {
		this.mealsName = mealsName;
	}
	
}
