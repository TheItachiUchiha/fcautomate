package com.infy.fc.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "ITEM")
public class Item {

	@Id
	@Column(name = "ITEM_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int itemId;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "VENDOR_FC_ID")
	private VendorFoodCourtMap foodCourtMap;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ITEM_TYPE_ID")
	private ItemType itemType;

	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MEALS_ID")
	private Meals meals;
	
	private String itemName;
	private char isAvailable;
	private float itemPrice;
    private boolean veg;
	public boolean isVeg() {
		return veg;
	}

	public void setVeg(boolean veg) {
		this.veg = veg;
	}

	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public VendorFoodCourtMap getFoodCourtMap() {
		return foodCourtMap;
	}

	public void setFoodCourtMap(VendorFoodCourtMap foodCourtMap) {
		this.foodCourtMap = foodCourtMap;
	}

	public ItemType getItemType() {
		return itemType;
	}

	public void setItemType(ItemType itemType) {
		this.itemType = itemType;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public char getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(char isAvailable) {
		this.isAvailable = isAvailable;
	}

	public float getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(float itemPrice) {
		this.itemPrice = itemPrice;
	}

}
