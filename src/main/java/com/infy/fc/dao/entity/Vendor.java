package com.infy.fc.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="VENDOR")
public class Vendor {
	
	@Id
	@Column(name="VENDOR_ID")
	 @GeneratedValue(strategy = GenerationType.AUTO)
	private int vendorId;
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	@Column(name="VENDOR_NAME")
	private String vendorName;
	@Column(name="PHONE_NUMBER")
	private String phoneNumber;

}
