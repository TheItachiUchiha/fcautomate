package com.infy.fc.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="ITEM_MEAL")
public class ItemMeal {

	@Id
	@Column(name="ITEM_MEAL_ID")
	 @GeneratedValue(strategy = GenerationType.AUTO)
	private int itemMealId;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ITEM_ID")
	private Item item;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MEALS_ID")
		private Meals meals;

	public int getItemMealId() {
		return itemMealId;
	}

	public void setItemMealId(int itemMealId) {
		this.itemMealId = itemMealId;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Meals getMeals() {
		return meals;
	}

	public void setMeals(Meals meals) {
		this.meals = meals;
	}
	
	
}
