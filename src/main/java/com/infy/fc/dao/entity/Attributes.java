package com.infy.fc.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ATTRIBUTES")
public class Attributes {

	@Id
	@Column(name="ATTRIBUTE_ID")
	 @GeneratedValue(strategy = GenerationType.AUTO)
	private int attributesId;
	@Column(name="ATTRIBUTES_NAME")
	private String attributesName;
	public int getAttributesId() {
		return attributesId;
	}
	public void setAttributesId(int attributesId) {
		this.attributesId = attributesId;
	}
	public String getAttributesName() {
		return attributesName;
	}
	public void setAttributesName(String attributesName) {
		this.attributesName = attributesName;
	}
	
}
