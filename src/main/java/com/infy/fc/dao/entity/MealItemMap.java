package com.infy.fc.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//@Entity
//@Table(name="MEAL_ITEM")
public class MealItemMap {
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "MEAL_ID")
	private Meals meal;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ITEM_ID")
	private Item item;


	public Meals getMeal() {
		return meal;
	}


	public void setMeal(Meals meal) {
		this.meal = meal;
	}


	public Item getItem() {
		return item;
	}


	public void setItem(Item item) {
		this.item = item;
	}
	
	
}
