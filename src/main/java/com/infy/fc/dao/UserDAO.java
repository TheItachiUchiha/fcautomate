package com.infy.fc.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.infy.fc.dao.entity.FoodCourt;
import com.infy.fc.dao.entity.Item;
import com.infy.fc.dao.entity.Location;
import com.infy.fc.dao.entity.Vendor;

@Transactional
@Repository
public class UserDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Location> getLocations(){
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Location"); //You will get Weayher object
		List<Location> list = query.list(); 
		return list;
	}
	
	public List<FoodCourt> getFCs(int dcID){
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select fc from FoodCourt as fc where fc.location.id=:dcID")
	            .setParameter("dcID",dcID); 
		List<FoodCourt> results = query.list();
		return results;
	}

	public List<Vendor> getVendorsForFc(int fcId) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("select v from Vendor as v where vendorId in "
				+ "(select vendor.id from VendorFoodCourtMap where FC_ID=:fc_id))")
	            .setParameter("fc_id",fcId); 
		List<Vendor> results = query.list();
		
		return results;
	}

	public List<Item> getFoodItemsDetailsGivenVendor_ID(int fc_id, int vendor_id, int mealType) {

		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("Select i from item as i where vendor_fc_id "
				+ "in (select v from VendorFoodCourtMap as v where foodCourt.foodCourtId=:fc_id and vendor.vendorId=:vendor_id)"
				+ " and meals.mealsId=:mealType")
				.setParameter("fc_id", fc_id)
	            .setParameter("vendor_id",vendor_id)
	            .setParameter("mealType", mealType); 
		List<Item> results = query.list();
		
		return results;
		
	}
}
