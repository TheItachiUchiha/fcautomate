package com.infy.fc.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infy.fc.VO.FoodCourtsVO;
import com.infy.fc.VO.ItemTypeVO;
import com.infy.fc.VO.ItemVO;
import com.infy.fc.VO.LocationVO;
import com.infy.fc.VO.VendorsVO;
import com.infy.fc.dao.UserDAO;
import com.infy.fc.dao.entity.FoodCourt;
import com.infy.fc.dao.entity.Item;
import com.infy.fc.dao.entity.ItemType;
import com.infy.fc.dao.entity.Location;
import com.infy.fc.dao.entity.Vendor;

@Service
public class UserService {

	@Autowired
	private UserDAO userDAO;
	
	public List<LocationVO> getLocations() {
		List<LocationVO> locations = new ArrayList<LocationVO>();
		try{
			List<Location> list = userDAO.getLocations();
			if(!locations.isEmpty() || null != locations){
				for(Location loc:list){
					LocationVO location = new LocationVO();
					location.setId(loc.getDcId());
					location.setName(loc.getDcName());
					locations.add(location);
				}
			} 
		}catch(Exception e){
			e.printStackTrace();
		}
		return locations;
	}

	public List<FoodCourtsVO> getFCs(int dcID){
		List<FoodCourtsVO> returnList = new ArrayList<FoodCourtsVO>();
		try {
			List<FoodCourt> resultSet=userDAO.getFCs(dcID);
			if(resultSet!=null && !resultSet.isEmpty()){
				Iterator<FoodCourt> ite=resultSet.iterator();
				while(ite.hasNext()){
					FoodCourtsVO foodCourtsVO = new FoodCourtsVO();	
					FoodCourt fc=ite.next();
					foodCourtsVO.setFoodCourtId(fc.getFoodCourtId());
					foodCourtsVO.setFoodCourtName(fc.getFoodCourtName());
					foodCourtsVO.setLocation(fc.getLocation());
					returnList.add(foodCourtsVO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnList;
	}


	public List<VendorsVO> getVendors(int fcId){
		List<VendorsVO> returnList = new ArrayList<VendorsVO>();
		try {
			List<Vendor> resultSet=userDAO.getVendorsForFc(fcId);
			if(resultSet!=null && !resultSet.isEmpty()){
				VendorsVO vendorsVO= new VendorsVO();	
				Iterator<Vendor> ite=resultSet.iterator();
				while(ite.hasNext()){
					Vendor ven=ite.next();
					vendorsVO.setVendorId(ven.getVendorId());
					vendorsVO.setVendorName(ven.getVendorName());
					vendorsVO.setPhoneNumber(ven.getPhoneNumber());
					returnList.add(vendorsVO);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return (ArrayList<VendorsVO>) returnList;
	}

	public List<ItemVO> selectFoodItems(int vendorId,int mealId, int fcId){

		List<ItemVO> returnList = new ArrayList<ItemVO>();
		try {
			List<Item> resultSet=userDAO.getFoodItemsDetailsGivenVendor_ID(fcId, vendorId, mealId);

			if(resultSet!=null && !resultSet.isEmpty()){

				ItemVO itemVO= new ItemVO();	
				Iterator<Item> ite=resultSet.iterator();
				while(ite.hasNext()){
					Item item=ite.next();
			
					itemVO.setItemId(item.getItemId());
					itemVO.setFoodCourtMap(item.getFoodCourtMap());
					itemVO.setItemType(getItemVO(item.getItemType()));
					itemVO.setVeg(item.isVeg());
					itemVO.setName(item.getItemName());
					itemVO.setIsAvailable(item.getIsAvailable());
					itemVO.setPrice(item.getItemPrice());
					returnList.add(itemVO);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		//	logger.info("Error while fetching Vendor details "+e.getMessage());
		}
		return returnList;
	
	}
	
	private ItemTypeVO getItemVO(ItemType itemType) {
		ItemTypeVO itemTypeVO = new ItemTypeVO();
		itemTypeVO.setId(itemType.getItemTypeId());
		itemTypeVO.setName(itemType.getItemTypeName());
		return itemTypeVO;
	}
	

	public int getFoodItems(int fcID, int VendorID, int dcID, int MealID){
		return 0;
	}
}
